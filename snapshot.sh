#!/bin/bash
#./snap.sh localhost:9200 my_fs_backup "*" false 5
set -x 
URI=$1
REPO=$2
INDICES=$3
GL_STATE=$4
LIMIT=$5
ARGS="$#"

TIMESTAMP=`date +%Y%m%d:%s`

create_backup() {
   curl -X PUT "$URI/_snapshot/$REPO/snapshot_$TIMESTAMP?wait_for_completion=true&pretty" -H 'Content-Type: application/json' -d'
   {
     "indices": "'"$INDICES"'",
     "partial": false,
     "ignore_unavailable": true,
     "include_global_state": '$GL_STATE'
   }
   '
}

check_args() {
 if [ $ARGS != 5 ];then
	echo "Bad number of ARGS"
	exit 1
 fi
}

rotate_backup() {
test=$(curl -m 30 -s -X GET "$URI/_snapshot/$REPO/_all?pretty" | jq .snapshots[:-$LIMIT][].snapshot)
 for snapshot in $test
 do
	name=$(echo $snapshot | tr -d '"')
	echo "Deleting snapshot: $name"
	curl -X DELETE "$URI/_snapshot/$REPO/$name?pretty"
 done
}


main () {
	check_args
	create_backup
	rotate_backup
}

main

